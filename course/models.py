# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class City(models.Model):
    name = models.CharField(max_length=30)
    postal_code = models.CharField(max_length=30)

    def __str__(self):              # __unicode__ on Python 2
        return self.name + self.postal_code


class Home(models.Model):
    street = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    city = models.ForeignKey(
        'City',
        on_delete=models.SET_NULL, null=True
    )

    def __str__(self):              # __unicode__ on Python 2
        return self.street + self.name


class Person(models.Model):
    # I left out name and surname because I will use the User module
    # name = models.CharField(max_length=30)
    # surname = models.CharField(max_length=30)
    user = models.OneToOneField(User, null=True)
    dni = models.SmallIntegerField()
    home = models.ForeignKey(
        'Home',
        on_delete=models.SET_NULL, null=True
    )
    # members = models.ManyToManyField(People)

    def __str__(self):              # __unicode__ on Python 2
        return self.dni

    def __unicode__(self):
        return u'%s' % (self.dni)


class Course(models.Model):
    name = models.CharField(max_length=30)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)

    def __str__(self):
        return self.name


class Career(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Student(Person):
    file = models.SmallIntegerField()
    enrolled_courses = models.ManyToManyField(Course)
    Careers = models.ManyToManyField(Career)

    def __str__(self):
        return str(self.file)


class Teacher(Person):
    degree = models.CharField(max_length=30)
    teaching_courses = models.ManyToManyField(Course)

    # TODO: Falta probar devolver cosas de la super clase
    def __str__(self):
        return self.degree
