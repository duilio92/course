# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import City, Teacher, Home, Person


class CityCreateForm():

    def __init__(self, values):
        """Metodo init."""
        self.name = values.get('name', None)
        self.postal_code = values.get('postal-code', None)

    def is_valid(self):
        """Metodo is valid."""
        if self.name and self.postal_code:
            if City.objects.all().filter(name=self.name).count():
                self.error = "La ciudad ya existe, elija otro nombre"
                return False
            elif self.postal_code == 0:
                self.error = "El codigo postal no puede ser 0"
                return False
            return True

        self.error = "Todos los campos son obligatorios"
        return False

    def save(self):
        """Metodo save."""
        city = City(
            name=self.name,
            postal_code=self.postal_code)
        city.save()
        return city


class CityUpdateForm(CityCreateForm, object):

    def __init__(self, values, id):
        """Metodo init."""
        super(CityUpdateForm, self).__init__(values)
        self.id = id
        self.cancel = values.get('cancel', None)

    def is_valid(self):
        """Metodo is_valid."""
        if self.cancel:
            return False
        if self.name and self.postal_code:
            if self.postal_code == 0:
                self.error = "El codigo postal no puede ser 0"
                return False
            return True

        self.error = "Todos los campos son obligatorios"
        return False

    def save(self):
        """Metodo save."""
        city = City.objects.get(pk=self.id)
        city.name = self.name
        city.postal_code = self.postal_code
        city.save()
        return city


class CityDeleteForm():

    def __init__(self, values, id):
        """Metodo init."""
        self.id = id
        self.cancel = values.get('cancel', None)

    def is_valid(self):
        """Metodo is_valid."""
        if self.cancel:
            return False
        if City.objects.get(pk=self.id):
            return True
        else:
            self.error = "No existe esa ciudad"
            return False

    def save(self):
        """Metodo save."""
        city = City.objects.get(pk=self.id)
        city.delete()


class TeacherCreateForm():

    def __init__(self, values):
        """Metodo init."""
        self.name = values.get('name', None)
        self.surname = values.get('surname', None)
        self.dni = values.get('dni', None)
        self.degree = values.get('degree', None)
        self.houses = values.get('houses', None)

    def is_valid(self):
        """Metodo is_valid."""
        if self.name and self.surname and self.dni and self.degree and self.houses:
            house = Home.objects.filter(pk=self.houses)
            if not house.count() > 0:
                self.error = "La casa ingresada no existe"
                return False
            elif self.dni == 0:
                self.error = "El DNI no puede ser 0"
                return False
        return True

        self.error = "Todos los campos son obligatorios"
        return False

    def save(self):
        """Metodo save."""
        house = Home.objects.get(pk=self.houses)
        t = Teacher(
            name=self.name,
            surname=self.surname,
            dni=self.dni,
            home=house,
            degree=self.degree)
        t.save()
        return Teacher


class TeacherUpdateForm(TeacherCreateForm, object):

    def __init__(self, values, id):
        """Metodo init."""
        super(TeacherUpdateForm, self).__init__(values)
        self.id = id
        # print vars(self)

    def is_valid(self):
        """Metodo is_valid."""
        if self.name and self.surname and self.dni and self.degree:
            house = Home.objects.filter(pk=self.houses)
            if not house:
                self.error = "La casa ingresada no existe"
                return False
            elif self.dni == 0:
                self.error = "El DNI no puede ser 0"
                return False
        return True

        self.error = "Todos los campos son obligatorios"
        return False

    def save(self):
        """Metodo save."""
        house = Home.objects.get(pk=self.houses)
        t = Teacher.objects.get(pk=self.id)
        t.name = self.name
        t.surname = self.surname
        t.dni = self.dni
        t.home = house
        t.degree = self.degree
        t.save()


class TeacherDeleteForm():

    def __init__(self, values, id):
        """Metodo init."""
        self.id = id
        self.cancel = values.get('cancel', None)

    def is_valid(self):
        """Metodo is_valid."""
        if self.cancel:
            return False
        if Teacher.objects.get(pk=self.id):
            return True
        else:
            self.error = "No existe ese profesor"
            return False

    def save(self):
        """Metodo save."""
        teacher = Teacher.objects.get(pk=self.id)
        teacher.delete()


class LoginForm():

    def __init__(self, values):
        """Metodo init."""
        self.username = values.get('username', None)
        self.password = values.get('password', None)

    def is_valid(self):
        """Metodo is_valid."""
        if self.username and self.password:
            # ver lo de que el usuario exista y tenga la contraseña bien
            return True
        else:
            self.error = "No existe ese profesor"
            return False

    def save(self):
        """Metodo save."""
