from django.conf.urls import url
# from . import views
from views import CitiesList, CityCreate, CityUpdate, CityDelete
from views import TeacherList, TeacherCreate, TeacherUpdate, TeacherDelete
from views import IndexView, TeacherHomeView, StudentHomeView

app_name = 'course'

urlpatterns = [
    # HOME PAGE
    url(r'home/$', IndexView.as_view(), name='index'),
    url(r'home-teacher/(?P<pk>[0-9]+)$', TeacherHomeView.as_view(), name='teacher-home'),
    url(r'home-student/(?P<pk>[0-9]+)$', StudentHomeView.as_view(), name='student-home'),
    # Home Page ends

    # Cities URLS BEGINS
    url(r'cities/$', CitiesList.as_view(), name='cities-list'),
    url(r'city/add/$', CityCreate.as_view(), name='city-add'),
    url(r'city/(?P<pk>[0-9]+)/$', CityUpdate.as_view(), name='city-update'),
    url(r'city/(?P<pk>[0-9]+)/delete/$', CityDelete.as_view(), name='city-delete'),
    # Cities ends

    # TEACHER URLS BEGINS
    url(r'teacher/$', TeacherList.as_view(), name='teacher-list'),
    url(r'teacher/add/$', TeacherCreate.as_view(), name='teacher-add'),
    url(r'teacher/(?P<pk>[0-9]+)/$', TeacherUpdate.as_view(), name='teacher-update'),
    url(r'teacher/(?P<pk>[0-9]+)/delete/$', TeacherDelete.as_view(), name='teacher-delete')
    # TEACHER ENDS

]
