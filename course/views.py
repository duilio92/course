# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# from django.shortcuts import render
# from django.http import HttpResponse

from course.models import City, Teacher, Course, Student
from course.models import Home
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView
from .forms import CityCreateForm, CityUpdateForm, CityDeleteForm
from .forms import TeacherCreateForm, TeacherUpdateForm, TeacherDeleteForm
from django.conf import settings
import datetime
import calendar
# from braces.views import LoginRequiredMixin


# CityViews Begin


class CityCreate(CreateView):
    model = City
    fields = ['name']
    # form_class = CityCreateForm
    template_name = 'course/city_form.html'

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = CityCreateForm(request.POST)
        if form.is_valid():
            form.save()
            # <process form cleaned data>
            return HttpResponseRedirect(reverse_lazy('course:cities-list'))
        ctx['error'] = form.error
        return render(request, self.template_name, ctx)


class CityUpdate(UpdateView):
    model = City
    template_name = 'course/city_update.html'
    form_class = CityUpdateForm
    success_url = reverse_lazy('course:cities-list')

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        print kwargs
        ctx = {}
        key = kwargs['pk']
        city = self.model.objects.get(pk=key)
        ctx['city'] = city
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        ctx['error'] = form.error
        return render(request, self.template_name, ctx)


class CityDelete(DeleteView):
    model = City
    success_url = reverse_lazy('course:cities-list')
    template_name = 'course/city_confirm_delete.html'
    form_class = CityDeleteForm

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        print kwargs
        ctx = {}
        key = kwargs['pk']
        city = self.model.objects.get(pk=key)
        ctx['city'] = city
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        ctx['error'] = form.error
        return render(request, reverse_lazy('course:cities-delete'), ctx)


class CitiesList(ListView):
    model = City
    template_name = 'course/city_list.html'

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        ctx = {}
        ctx['cities'] = self.model.objects.all()
        return render(request, self.template_name, ctx)

# CityViews End

# TeacherViews Begin


class TeacherList(ListView):
    model = Teacher
    template_name = 'course/teacher_list.html'

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        ctx = {}
        ctx['teachers'] = self.model.objects.all()
        return render(request, self.template_name, ctx)


class TeacherCreate(CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'course/teacher_form.html'
    success_url = reverse_lazy('course:teacher-list')

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        ctx = {}
        ctx['houses'] = Home.objects.filter()
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        ctx['error'] = form.error
        return render(request, self.template_name, ctx)


class TeacherUpdate(UpdateView):
    model = Teacher
    form_class = TeacherUpdateForm
    template_name = 'course/teacher_update.html'
    success_url = reverse_lazy('course:teacher-list')

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        print kwargs
        ctx = {}
        ctx['houses'] = Home.objects.all()
        key = kwargs['pk']
        teacher = self.model.objects.get(pk=key)
        ctx['teacher'] = teacher
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        ctx['error'] = form.error
        return render(request, self.template_name, ctx)


class TeacherDelete(DeleteView):
    model = Teacher
    success_url = reverse_lazy('course:teacher-list')
    template_name = 'course/teacher_confirm_delete.html'
    form_class = TeacherDeleteForm

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        ctx = {}
        key = kwargs['pk']
        teacher = self.model.objects.filter(pk=key)
        if teacher.count() > 0:
            ctx['teacher'] = teacher[0]
        else:
            ctx['no_teacher'] = True
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        """Metodo post."""
        ctx = {}
        form = self.form_class(request.POST, kwargs['pk'])
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        ctx['error'] = form.error
        return render(request, reverse_lazy('course:cities-delete'), ctx)

# TeacherViewsEnd

# home view start


class IndexView(ListView):

    def get(self, request, *args, **kwargs):
        """Metodo get."""
        user = request.user
        if request.user.is_authenticated:
            if 'teacher' in user.groups.all().values_list('name', flat=True):
                return HttpResponseRedirect(reverse_lazy('course:teacher-home', kwargs={'pk': user.pk}))
            if 'student' in user.groups.all().values_list('name', flat=True):
                return HttpResponseRedirect(reverse_lazy('course:student-home', kwargs={'pk': user.pk}))
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)


class TeacherHomeView(ListView):
    template_name = 'teacher_home.html'

    def get(self, request, *args, **kwargs):
        """Show teacher-home page."""
        # pk = request.user.pk
        print 'llegue al teacher-home'


class StudentHomeView(ListView):
    template_name = 'student_home.html'

    def starting_courses(self):
        """Return the courses that start in this month."""
        today = datetime.date.today()
        first_day_month = datetime.date(today.year, today.month, 1)
        range_month = calendar.monthrange(today.year, today.month)
        last_day_month = datetime.date(today.year, today.month, range_month[-1])
        return Course.objects.filter(start_date__range=[first_day_month, last_day_month])

    def enrolled_courses(self, pk):
        """Return the enrolled courses of the user received as parameter."""
        s = Student.objects.get(user__pk=pk)
        return s.enrolled_courses.all()

    def get(self, request, *args, **kwargs):
        """Show student-home page."""
        starting_courses = self.starting_courses()
        pk = request.user.pk
        enrolled_courses = self.enrolled_courses(pk)
        print 'llegue al student-home'

# home views end
