# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Course, Teacher, Student, Career, City, Home, Person
# Register your models here.

admin.site.register(Person)
admin.site.register(Course)
admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(Career)
admin.site.register(City)
admin.site.register(Home)
